import React, {useEffect, useState} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'
import axios from './services/axios'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment'





export default function App() {
    const [isFormActive, setIsFormActive] = useState(false)
    const [id, setId] = useState([])
    const [tarefas, setTarefas] = useState([])
    const [titulo, setTitulo] = useState('')
    const [descricao, setDescricao] = useState('')
    const [responsavel, setResponsavel] = useState('')
    const [data_de_entrega, setDataDeEntrega] = useState('')
    const [status, setStatus] = useState('')

    useEffect(() => {
        async function carregarTaregas(){
            const res = await axios.get('/tarefas')
            setTarefas(res.data)
        }
        carregarTaregas()
    }, [])

    const printErrors = errors => {
        const theErrors = errors.response.data
        for(let i in theErrors)
            alert(theErrors[i])
    }


    //CRUD METHODS

    const inserir = function(){
        setId(null)
        setTitulo('')
        setDescricao('')
        setResponsavel('')
        setDataDeEntrega('')
        setStatus('')
        setIsFormActive(true)
    }
    const editar = function(id){
        setId(id)
        let dataFormatada = null
        const tarefaData = tarefas.filter(f => f.id == id)[0]

        if(tarefaData.data_de_entrega){
            dataFormatada = new Date(tarefaData.data_de_entrega)
            dataFormatada.setDate(dataFormatada.getDate() + 1)
        }
        setTitulo(tarefaData.titulo)
        setDescricao(tarefaData.descricao)
        setResponsavel(tarefaData.responsavel)
        setDataDeEntrega(dataFormatada)
        setStatus(tarefaData.status)
        setIsFormActive(true)
    }
    const excluir = function(id){
        axios.delete(`/tarefas/${id}`)
            .then(res => setTarefas(tarefas.filter(f => f.id != id)))
    }

    const salvar = function(){
        const dataFormatada = moment(data_de_entrega).format('Y-M-D')

        const data = {
            titulo: titulo,
            descricao: descricao,
            responsavel: responsavel,
            data_de_entrega: (dataFormatada === 'Invalid date') ? null : dataFormatada,
            status: status,
        }

        if(!id){
            axios.post('/tarefas', data)
                .then(res => {
                    setTarefas([...tarefas, res.data])
                    setIsFormActive(false)
                })
                .catch(printErrors)
        }else{
            axios.put(`/tarefas/${id}`, data)
                .then(res => {
                    const tarefasAtualizadas = tarefas.map(f => (f.id == id) ? res.data : f)
                    setTarefas(tarefasAtualizadas)
                    setIsFormActive(false)
                })
                .catch(printErrors)
        }
    }

    const renderTable = () => {
        return (
            <>
                <button className="btn btn-primary" onClick={e => inserir()}>Nova Tarefa</button>
                <table className="table mt-4">
                    <thead>
                    <tr>
                        <th scope="col">Título</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Responsavel</th>
                        <th scope="col">Data de entrega</th>
                        <th scope="col">Status</th>
                        <th scope="col">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {tarefas.map(cb => (
                        <tr key={cb.id}>
                            <td>{cb.titulo}</td>
                            <td>{cb.descricao}</td>
                            <td>{cb.responsavel}</td>
                            <td>{cb.data_de_entrega ? moment(cb.data_de_entrega).format('DD/MM/YYYY') : ''}</td>
                            <td>{cb.status}</td>
                            <td>
                                <button className="btn btn-info" onClick={e => editar(cb.id)}>Editar</button>
                                <button className="btn btn-danger ml-2" onClick={e => excluir(cb.id)}>Excluir</button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </>
        )
    }

    const renderForm = () => {
        if(isFormActive)
            return (
                <div className="form">
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Título</label>
                                <input type="text" className="form-control"
                                       name="titulo" value={titulo} onChange={e => setTitulo(e.target.value)}
                                       placeholder="Ex: Correção">
                                </input>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Descrição</label>
                                <textarea type="text" className="form-control"
                                       name="descricao" value={descricao} onChange={e => setDescricao(e.target.value)}
                                       placeholder="Ex: Analisar e corrigir a planilha do financeiro.">
                                </textarea>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Responsavel</label>
                                <input type="text" className="form-control"
                                       name="responsavel" value={responsavel} onChange={e => setResponsavel(e.target.value)}
                                       placeholder="Ex: Witalo">
                                </input>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Data de entrega</label>
                                <DatePicker className="form-control w-100" selected={data_de_entrega} onChange={e => setDataDeEntrega(e)}/>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Status</label>
                                <input type="text" className="form-control"
                                       name="status" value={status} onChange={e => setStatus(e.target.value)}
                                       placeholder="Ex: Pendente">
                                </input>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-12 d-flex justify-content-end">
                            <button className="btn btn-primary" onClick={e => salvar()}>Salvar</button>
                            <button className="btn btn-secondary ml-2" onClick={e => setIsFormActive(false)}>Cancelar</button>
                        </div>
                    </div>
                </div>

            )
    }

    return (
        <div>
            {renderForm()}
            {renderTable()}
        </div>

    );
}

